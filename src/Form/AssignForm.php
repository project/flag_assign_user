<?php

namespace Drupal\flag_assign_user\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\flag\FlagService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Flag Assign User form.
 */
class AssignForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Flag service.
   *
   * @var \Drupal\flag\FlagService
   */
  protected $flagService;

  /**
   * Constructor for the dependency injection container.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\flag\FlagService $flag_service
   *   The Flag service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FlagService $flag_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->flagService = $flag_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('entity_type.manager'),
    $container->get('flag')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flag_assign_user_assign';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $flag = '', $id = NULL) {
    $form['user'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('User'),
      '#description' => $this->t('Select a user to assign.'),
      '#default_value' => '',
      '#tags' => TRUE,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    // @todo Conditionally show a select, radios, or links if no value in URL.
    // OPTION: If only one flag defined, automatically use?.
    // @todo Throw an error and/or selection option as above for invalid id.
    $flag_service = $this->flagService;
    $flag_obj = $flag_service->getFlagById($flag);
    $form['flag'] = [
      '#type' => 'value',
      '#value' => $flag,
    ];
    // Store the flag's target entity type, for later use.
    $form['type'] = [
      '#type' => 'value',
      '#value' => $flag_obj->getFlaggableEntityTypeId(),
    ];
    // @todo Conditionally show an autocomplete if no value in URL.
    $form['id'] = [
      '#type' => 'value',
      '#value' => $id,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Assign'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo Validate that specified entity is of an allowed bundle, per the
    // flag definition.
    // @todo Validate that specified user can flag content?
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $flag_id = $form_state->getValue('flag');
    $type = $form_state->getValue('type');
    if ($id = $form_state->getValue('id')) {
      $entity = $this->entityTypeManager->getStorage($type)->load($id);

      $user_select = $form_state->getValue('user');
      if (is_array($user_select)) {
        $user_select = array_pop($user_select);
      }
      if ($user_select['target_id']) {
        $user = $this->entityTypeManager->getStorage('user')->load($user_select['target_id']);
        $flag_service = $this->flagService;
        $flag = $flag_service->getFlagById($flag_id, $entity, $user);
        if ($flag->isFlagged($entity, $user)) {
          $flag_service->unflag($flag, $entity, $user);
        }
        else {
          $flag_service->flag($flag, $entity, $user);
        }
      }
    }
  }

}
