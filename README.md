# Flag Assign User

This module allows privileged users to toggle a flag on behalf of other users by
accessing a specially crafted URL, for example:
`https://mydomain.com/flag-assign-user/assign/bookmark/97`

Note that the last two parts of the URL are meant to provide:
- the machine name of the flag to use (e.g. bookmark)
- the entity ID that should be flagged (entity type is predefined for the flag)

Note that this module defines a new permission, assign users to flag, that is
required to use the provided functionality. Also note that the processing treats
the request as a toggle, so a user who already has a flag on a content entity
will have that unflagged by the request.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/flag_assign_user).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/flag_assign_user).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the Flag module to be installed, and at least one flag
defined.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module currently provides no configuration options.


## Maintainers

- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)